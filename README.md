# README #

### What is this repository for? ###

This generates a book from a bunch of "book pieces". It uses python DOCX to read and write docx files. It generates the final book as a word document. 
### How do I get set up? ###

You have to have docx.
You have to have a word document that has all the styles in it called "template.docx" (and example is provided)
You have to have a schema file. This is very specific to my book.

So this isn't ready for prime time yet because it doesn't play nice with others. 
And I'm not really sure that this is a technique anyone would go for. 

However, if you had the schema generator you could use that to generate your schema for the book (list of parts and how they should be formatted) and if you had the index generator (still working on that) you could generate the indexes. 

Then you'd have to twist the directories around and it would do OK. 

I need to make it a lot more general to be useful to anyone else. 

### Contribution guidelines ###

Currently I don't think this is useful to anyone but me but I think the CONCEPT might be interesting. 

### Who do I talk to? ###

fatzer at google mail service dot com