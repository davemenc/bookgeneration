from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH
from datetime import date
import string

import inspect
import error
import os
import pprint

def Even(x):
	return x%2==0

def FilterStr(s):
	return ''.join(filter(lambda c:c in string.printable, s))

def EmitRuns(str,OutDoc,Style):
	StrLen=len(str)
	list = [0]
	LimitCount=0
	while True:
		LimitCount+=1
		if LimitCount>10: break
		m=str[list[-1]:].find("~")
		if m==-1:
			m=StrLen
		else: m=m+list[-1]
		list.append(m+1)
		if m>=StrLen:
			break
	p = OutDoc.add_paragraph('',style=Style)
	for i in range(0,len(list)-1):
		if Even(i):
			p.add_run(str[list[i]:list[i+1]-1])
		else: 
			Operator = str[list[i]]
			Run=str[list[i]+1:list[i+1]-1]
			if Operator=='b': #bold
				p.add_run(Run).bold = True #bold
			elif Operator=='i':
				p.add_run(Run).italic = True #italic
			elif Operator=='s': # superscript (not implemented or used)
				pass
			elif Operator=='d': #inDented about .25 inch
				p.add_run(Run)
				p.paragraph_format.left_indent = Inches(0.50)
			else:
				print "~~~~~~ Error, found operator "+Operator+" which is NOT i, s, or b; trust me. "+" in text "+FilterStr(str)

#a few constants
INDEX = 0
TYPE = 1
FILE = 2
IMAGE = 3
TEXT = 4

ExampleKeys = ["Title:","Date:","Time:","Location:","Ascendant:","Author:","Text:"]

def ParseDate(datestr):
	#print "ParseDate()",datestr
	firstdash = datestr.find("-")
	seconddash=datestr[firstdash+1:].find("-")+firstdash
	year = int(datestr[0:firstdash])
	month =  int(datestr[firstdash+1:seconddash+1])
	day = int(datestr[seconddash+2:])
	d=date.today().replace(year, month, day)
	d.replace(year, month, day)
	return d.strftime("%B %d, %Y")

def ParseTime(timestr):
	Hours = int(timestr[0:2])
	Minutes = timestr[3:]
	Suffix = ""
	if Hours == 12 and Minutes=="00":
		Suffix = " Noon"
	elif Hours > 11:
		Suffix = " P.M."
		if Hours > 12:
			Hours += -12
	else: 
		Suffix = " A.M."
	Result = str(Hours)+":"+Minutes+Suffix
	return Result
	
def Example(params,output,schemaline,SourceDir):
	#print "Example",params,output
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		ifname = SourceDir+params[IMAGE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		PreText = True # we haven't found the TEXT: key yet
		Title=Date=Time=Location=Ascendant=Author=""
		for para in document.paragraphs:
			if PreText:
				Section = para.text
				KeyMark = Section.find(":")
				if KeyMark == -1: #mark not found; skip it
					error.EmitError(params[TYPE]+" has missing key mark at start.",schemaline)
					continue
				KeyName = Section[0:KeyMark]
				Section = Section[KeyMark+1:len(Section)].strip()
				if KeyName=="Title":
					Title = Section
				elif KeyName== "Date":
					Date = Section
				elif KeyName== "Time":
					Time = Section
				elif KeyName== "Location":
					Location = Section
				elif KeyName== "Ascendant":
					Ascendant = Section
				elif KeyName== "Author":
					Author = Section
				elif KeyName== "Text":
					PreText=False
					output.add_paragraph(Title,style="Heading 2")
					output.add_paragraph(Author,style="ByLine")
					if os.path.isfile(ifname):
						output.add_picture(ifname, width=Inches(5))
						last_paragraph = output.paragraphs[-1] 
						last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
					else:
						error.EmitError(params[TYPE]+" file "+ifname+" does not exist.",schemaline)
					dateline = ParseDate(Date)+", "+ParseTime(Time)+" | "+Location +" | "+Ascendant
					#print Date,schemaline,dateline
					output.add_paragraph(dateline, style='Dateline')
			else: #not preText
				EmitRuns(para.text,output,'ExampleBody' )
				#output.add_paragraph(para.text,style='ExampleBody')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)
	output.add_page_break()
	

def Book_Title(params,output,schemaline,SourceDir):
	#Book_Title -- just write TEXT out

	#print params[TYPE],params,output
	if len(params)>TEXT:
		output.add_paragraph(params[TEXT],style="BookTitle")
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)
	
def Chapter_Title(params,output,schemaline,SourceDir):
	# Chapter_Title -- just write TEXT out
	#print params[TYPE],params,output

	#output.add_page_break()
	if len(params)>TEXT:
		output.add_paragraph( params[TEXT], style='Heading 1')
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)

def Introduction(params,output,schemaline,SourceDir):
	#print "Introduction",params,output
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			if para.text[0:2] == "By":
				output.add_paragraph(para.text, style='ByLine')
			else:
				EmitRuns(para.text,output,'Introduction')
				#output.add_paragraph(para.text, style='Introduction')
		output.add_page_break()

	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)

def Index(params,output,schemaline,SourceDir):
	#print "Index",params,output
	output.add_page_break()
	Title = "Index"
	if len(params)> TEXT:
		Title = params[TEXT]
	output.add_paragraph(Title,style="Heading 1")

	if len(params)>FILE: 
		fname = params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		infile = open(fname,"r")
		for line in infile:
			fields = line.strip().split("\t")
			if len(fields)<2: continue
			Word = fields[0]
			PageList = fields[1]
			output.add_paragraph( Word+" "+PageList,style="IndexWord")
			#output.add_paragraph(PageList,style="IndexPage")
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)

def Glossary(params,output,schemaline,SourceDir):
	#print "Glossary",params,output
	LocalGlossary = Document("D:/Google/Reading&Writing/HoraryExamples/BookCopy/horarytemplate.docx")

	output.add_page_break()
	Title = "Glossary"
	if len(params)> TEXT:
		Title = params[TEXT]
	#print "Title",Title
	output.add_paragraph(Title,style="Heading 1")
	LocalGlossary.add_paragraph(Title,style="Heading 1")
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		infile = open(fname,"r")
		for line in infile:
			fields = line.strip().split("\t")
			if len(fields)<2: continue
			Word = fields[0]
			Definition = fields[1]
			#print FilterStr(Word),FilterStr(Definition)
			
			output.add_paragraph( FilterStr(Word),style="GlossaryWord")
			EmitRuns(FilterStr(Definition),output,Style="GlossaryDefinition")
			#output.add_paragraph(FilterStr(Definition),style="GlossaryDefinition")
			LocalGlossary.add_paragraph( FilterStr(Word),style="GlossaryWord")
			EmitRuns(FilterStr(Definition),LocalGlossary,Style="GlossaryDefinition")
			#LocalGlossary.add_paragraph(FilterStr(Definition),style="GlossaryDefinition")
		LocalGlossary.save("LocalGlossary.docx")	
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)

def Credits(params,output,schemaline,SourceDir):
	#print "Credits",params,output
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			output.add_paragraph(para.text, style='Credits')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)

def Dedication(params,output,schemaline,SourceDir):
	#print params[TYPE],params,output
	output.add_page_break()
	output.add_paragraph("Dedication",style="Heading 1")
	if len(params)>TEXT:
		output.add_paragraph( params[TEXT], style='Dedication')
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)

def Copyright(params,output,schemaline,SourceDir):
	#print "Copyright",params,output
	output.add_paragraph('', style='Copyright')
	output.add_paragraph('', style='Copyright')
	output.add_paragraph('', style='Copyright')
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			output.add_paragraph(para.text, style='Copyright')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)

def Contents(params,output,schemaline,SourceDir):
	print "Contents",params,output
	output.add_page_break()
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			output.add_paragraph(para.text, style='TOC_Chapter')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)
	
def Acknowledgements(params,output,schemaline,SourceDir):
	#print "Acknowledgements",params,output
	output.add_page_break()
	if len(params)>TEXT:
		output.add_paragraph( params[TEXT], style='Heading 1')
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			EmitRuns(para.text,output,'Acknowledgement')
			#output.add_paragraph(para.text, style='Acknowledgement')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)
	output.add_page_break()


def Book_Subtitle(params,output,schemaline,SourceDir):
	#print params[TYPE],params,output
	if len(params)>TEXT:
		output.add_paragraph( params[TEXT], style='Subtitle')
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)

def ByLine(params,output,schemaline,SourceDir):
	#print params[TYPE],params,output
	if len(params)>TEXT:
		#output.add_paragraph( params[TEXT], style='ByLine')
		output.add_paragraph( params[TEXT])
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)

def Biographies(params,output,schemaline,SourceDir):
	#print "Biographies",params,output
	output.add_page_break()
	if len(params)>TEXT:
		output.add_paragraph( params[TEXT], style='Heading 1')
	else:
		error.EmitError(params[TYPE]+" has no TEXT parameter.",schemaline)
	if len(params)>FILE:
		fname = SourceDir+params[FILE]
		#print params[TYPE]+" file: ",fname
		if not os.path.isfile(fname):
			error.EmitError(params[TYPE]+" file "+fname+" does not exist.",schemaline)
			return -1
		document = Document(fname)
		for para in document.paragraphs:
			#output.add_paragraph('', style='Normal')
			EmitRuns(para.text,output,'Normal')
	else:
		error.EmitError(params[TYPE]+" has no FILE parameter.",schemaline)
