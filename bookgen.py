import sys
import output 
import error
from docx import Document

CopyRoot = "D:/Google/Reading&Writing/HoraryExamples/BookCopy/"
CopyCurrent = "D:/Google/Reading&Writing/HoraryExamples/BookCopy/FinalEdit_2/"

SchemaFile = sys.argv[1]
TemplateFile = CopyRoot+"horarytemplate.docx"
OutputFile = sys.argv[2]


print "SchemaFile: ",SchemaFile
print "OutputFile: ",OutputFile
print "TemplateFile: ", TemplateFile

OutputDocx = Document(TemplateFile)

FuncList = {'Example':output.Example,
	'Book_Title':output.Book_Title,
	'Chapter_Title':output.Chapter_Title,
	'Introduction':output.Introduction,
	'Index':output.Index,
	'Glossary':output.Glossary,
	'Credits':output.Credits,
	'Dedication':output.Dedication,
	'Copyright':output.Copyright,
	'Contents':output.Contents,
	'Acknowledgements':output.Acknowledgements,
	'Book_Subtitle':output.Book_Subtitle,
	'ByLine':output.ByLine,
	'Biographies':output.Biographies}
f = open(SchemaFile)

CurLine = 0
for line in iter(f):
	CurLine += 1
	#print CurLine,line
	words = line.split("\t")
	for j in range(0,len(words)):
		words[j]=words[j].strip()
	if len(words)<=output.INDEX:
		error.EmitError("No index in schemafile "+SchemaFile,CurLine)
	if len(words)>output.TYPE:
		Type = words[output.TYPE]
		if Type in FuncList:
			FuncList[words[output.TYPE]](words,OutputDocx,CurLine,CopyCurrent)
		else:
			error.EmitError(Type+" not in FuncList",CurLine)
	else: 
		error.EmitError("no Type",CurLine)
		
#print "Saving File to ",	OutputFile	
OutputDocx.save(OutputFile)	
print "Save file to ", OutputFile