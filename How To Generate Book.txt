How to Generate The book
1) We need a list of examples. 
  1.1) Go to exampleslist.xlsx. 
  1.2) Filter for those example you want (all of them, for example). 
  1.4) Select the fields on the far right "Example", "image", "Title", "Category", "order", "first page" -- be sure to get all rows and columns
  1.5) Copy them and paste them into the D:\Google\DEV\genschema\examplelist.txt file
2) Make sure the pre- and post-schema files in genschema are correct and what you want.
3) Run D:\Google\DEV\genschema\genschema.bat
4) Make sure there were no errors (check results.html)
5) run D:\Google\DEV\BookGeneration\bookgen.bat - be sure to close the book if it's open
6) correct any errors (as shown in Results.html)
7) Open the book 
8) generate a table of contents before the introduction
  8.1) Select References tab
  8.2) Click on "table of conents"
  8.3) select table of contents
  8.4) Copy and paste it into TOC.txt file in D:\Google\DEV\genindex
9) Generate the general index
  9.1) go to D:\Google\DEV\genindex
  9.2) run the geninidex.bat file
    9.2.1) This assumes we have a list of examples (examplelist.txt) in genschema
    9.2.2) Check that the schema generated is correct
    9.2.3) Check that the Toc.txt is correct
    9.2.4) The .bat file has all the correct files: examplelist, vocabulary, toc.txt
  9.3) Sort the index alphabetically
  9.4) Check the index carefully; may have to tweak the vocabulary file.
10) Generate the author index
11) Now you have to regenerate the book to include the indexes
12) Now regenerate the TOC in the word doc


