def lineno():
    """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno
    
def EmitError(ErrorText,line=0):
	print "***** ERROR:",ErrorText,
	if line==0:
		print 
	else:
		print "; input line",line
