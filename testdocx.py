from docx import Document
from docx.shared import Inches

recordset = [ [1, "101", "Spam"], [2, "42", "Eggs"], [3, "631", "Spam, spam, eggs, and spam"]]

document = Document("horarytemplate.docx")
document.add_heading('Document Title', 0)
document.add_picture('hope..jpg', width=Inches(6))


p = document.add_paragraph('A plain paragraph having some ')
p.add_run('bold').bold = True
p.add_run(' and some ')
p.add_run('italic.').italic = True

document.add_heading('Heading, level 1', level=1)
document.add_paragraph('ExampleTitle', style='ExampleTitle')
document.add_paragraph('Title', style='Title')
document.add_paragraph('ChapterTitle', style='ChapterTitle')
document.add_paragraph('Subtitle', style='Subtitle')
#document.add_paragraph('Byline', style='Byline')
document.add_paragraph('Normal', style='Normal')



document.add_page_break()

document.save('demo.docx')